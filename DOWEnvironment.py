import numpy as np
import pandas as pd
from Environment import Environment

class DOWEnvironment(Environment):
    def __init__(self, prices = './data/data.csv', capital = 1e6):
        self.prices = prices  
        self.capital = capital  
        self.data = self.loadData()

    def loadData(self):
        data =  pd.read_csv(self.prices)
        data.index = pd.to_datetime(data['date'])
        data = data.sort_index()
        del data['date']         
        return data[data.columns[:32]]

    def getReward(self, action, t_1, t):
        ret, sharpe, rew = self.getRewardPrev(action, t_1, t)
        return ret, rew