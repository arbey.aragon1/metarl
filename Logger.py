import logging
import os
import sys
from datetime import datetime
import configparser

class Logger():
    __instance = None

    def __init__(self, fileName):
        if Logger.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            Logger.__instance = self
            self.config = configparser.ConfigParser()

    @staticmethod
    def getInstance(fileName='log.log'):
        if Logger.__instance == None:
            Logger(fileName)
        return Logger.__instance 

    def debug(self, key, data):
        self.config.read('log.log')
        self.config['DEFAULT']['DATE'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        self.config['DEFAULT'][key] = str(data)
        with open('log.log', 'w') as configfile:
            self.config.write(configfile)
