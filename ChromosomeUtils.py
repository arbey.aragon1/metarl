import numpy as np
import pandas as pd

from Chromosome import Chromosome, ChromosomeLayer
from Utils import Utils
class ChromosomeUtils():
    ## Network
    hiddenLayerCount = {'type': 'minMaxInt', 'min': 1, 'max': 10, 'delta': 1}
    hiddenLayerNeurons = {'type': 'choise', 'values': [8, 16, 24, 32, 64, 128, 256, 512]}#
    hiddenLayerActivations = {'type': 'choise', 
                            'values': [
                                'tanh', 'relu', 'sigmoid', 
                                'linear', 'softmax',  
                                'softsign', 'selu', 'elu',
                                'exponential', 'softplus'
                            ]}#
    hiddenOptimizer = {'type': 'choise', 'values': ['adam', 'rmsprop', 'sgd']}
    hiddenLayerType = {'type': 'choise', 'values': ['dense', 'lstm' 'denseDropout', 'lstmDropout']}

    hiddenLoss = {'type': 'choise', 'values': ['mean_squared_error', 
                                            'mean_squared_logarithmic_error', 
                                            'mean_absolute_error',
                                            'binary_crossentropy','hinge','squared_hinge','categorical_crossentropy', 'sparse_categorical_crossentropy','kullback_leibler_divergence'
                                            ]}#

    eeMu = {'type': 'minMaxFloat', 'min': 0.0, 'max': 1.0, 'decimals': 3}
    eeSigma = {'type': 'minMaxFloat', 'min': 0.0, 'max': 0.1, 'decimals': 3}

    prefixUnits = 'units_'

    pMut = {'p0': 0.1,
        'p1': 0.1,
        'p2': 0.2,
        'p3': 0.5,
        'p4': 0.5,
        'p5': 0.1}

    pCross = {'p0': 0.5,
        'p1': 0.2,
        'p2': 0.6,
        'p3': 0.2,
        'p4': 0.6,
        'p5': 0.5}

    def __init__(self):
        pass

    def generateItem(self, item):
        if(item['type'] == 'minMaxFloat'):
            return np.round(np.random.uniform(low=item['min'], high=item['max'], size=(1,))[0], item['decimals'])
        elif(item['type'] == 'minMaxInt'):
            return np.random.randint(item['min'], item['max'] + 1, size=(1,))[0]
        elif(item['type'] == 'choise'):
            return np.random.choice(item['values'], 1)[0]
        
    def generateChromosomeLayer(self):
        layer = ChromosomeLayer()
        layer.SetType(self.generateItem(self.hiddenLayerType))
        layer.SetUnits(self.generateItem(self.hiddenLayerNeurons))
        layer.SetActivation(self.generateItem(self.hiddenLayerActivations))
        #if(layer['type'] == 'dense'):
        #    pass
        #elif(layer['type'] == 'lstm'):
        #    pass
        #elif(layer['type'] == 'gru'):
        #    pass
        return layer

    def generateChromosome(self):
        chromosome = Chromosome()
        chromosome.SetOptimizer(self.generateItem(self.hiddenOptimizer))
        
        chromosome.SetLearningRateWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        chromosome.SetDropoutWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        
        chromosome.SetGammaWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        chromosome.SetAlphaWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        chromosome.SetEpsilonDecayWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        chromosome.SetEpsilonMinWS([self.generateItem(self.eeMu), self.generateItem(self.eeSigma)])
        
        chromosome.SetLoss(self.generateItem(self.hiddenLoss))
        
        layers = {}
        for i in range(1,5+1):
            layers[self.prefixUnits+str(i)] = self.generateChromosomeLayer()
        chromosome.SetLayers(layers)
        return chromosome

    def __crossChromosomes(self, chr1, chr2, isMutation = False):
        h1, h2 = chr1.Copy(), chr2.Copy()
        if(isMutation):
            p = self.pMut
        else:
            p = self.pCross

        if(np.random.rand() <= p['p0']):
            h1.SetLoss(chr2.GetLoss())
            h2.SetLoss(chr1.GetLoss())
        if(np.random.rand() <= p['p1']):
            h1.SetOptimizer(chr2.GetOptimizer())
            h2.SetOptimizer(chr1.GetOptimizer())
        if(np.random.rand() <= p['p2']):
            if(np.random.rand() <= p['p3']):
                h1.SetLayers(h2.GetLayers())
                h2.SetLayers(h1.GetLayers())
            else:
                lp1 = h1.GetLayers()
                lp2 = h2.GetLayers()
                kp1 = np.random.choice(list(lp1.keys()))
                kp2 = np.random.choice(list(lp2.keys()))
                l1 = lp1[kp1]
                l2 = lp2[kp2]
                lp1[kp1] = l2
                lp2[kp2] = l1
                h1.SetLayers(lp1)
                h2.SetLayers(lp2)

        if(np.random.rand() <= p['p4']):
            if(not isMutation):
                if(np.random.rand() <= p['p5']):
                    w1, w2 = Utils.GeneExchangeCrossover(h1.GetW(), h2.GetW())
                    s1, s2 = Utils.GeneExchangeCrossover(h1.GetS(), h2.GetS())
                    h1.SetW(w1)
                    h2.SetW(w2)
                    h1.SetS(s1)
                    h2.SetS(s2)
                else:
                    w1 = Utils.MeanCrossing(h1.GetW(), h2.GetW())
                    s1 = Utils.MeanCrossing(h1.GetS(), h2.GetS())
                    h1.SetW(w1)
                    h1.SetS(s1)
            else:
                w = Utils.MutationEE(h1.GetW(), h1.GetS())
                h1.SetW(w)
        return h1, h2
                    
    def crossChromosomes(self, chr1, chr2):
        return self.__crossChromosomes(chr1, chr2)

    def mutationChromosomes(self, chr1):
        chr2 = self.generateChromosome()
        h1, _ = self.__crossChromosomes(chr1, chr2, isMutation = True)
        return h1