import os 
import json
import requests
import warnings
warnings.filterwarnings('ignore')

from keras.layers import Input, Dense, Flatten, Dropout
from keras.models import Model
from keras.regularizers import l2

import tensorflow  as tf
import numpy as np
import pandas as pd
import random
from collections import deque
import matplotlib
import matplotlib.pylab as plt

import statsmodels.api as sm
from statsmodels import regression
import seaborn as sns

from Chromosome import Chromosome, ChromosomeLayer
from ChromosomeUtils import ChromosomeUtils
from Utils import Utils

from PopulationManager import PopulationManager

from Coach import Coach
from DOWEnvironment import DOWEnvironment
from GeneticEnvironment import GeneticEnvironment

from Logger import Logger

sns.set(style="whitegrid")
current_cmap = matplotlib.cm.get_cmap()
current_cmap.set_bad(color='red')


logger = Logger.getInstance()

env = DOWEnvironment()

coach = Coach(logger, env, 10, debugFunction = True)

chromosomeUtils = ChromosomeUtils()

populationManager = PopulationManager(logger, coach, chromosomeUtils, populationSize = 10)

geneticEnvironment = GeneticEnvironment(logger, populationManager, 1000)

geneticEnvironment.Run()