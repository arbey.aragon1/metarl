import numpy as np
import pandas as pd

import random

from keras.layers import Input, Dense, Flatten, Dropout
from keras.models import Model
from keras.regularizers import l2
from tensorflow import keras
import tensorflow  as tf

from PortfolioCalculator import PortfolioCalculator

class Agent:
    def __init__(
                     self, 
                     portfolio_size,
                     chromosome,
                     is_eval = False, 
                     allow_short = True,
                 ):
        
        self.portfolio_size = portfolio_size
        self.allow_short = allow_short
        self.input_shape = (portfolio_size, portfolio_size,)
        self.action_size = 3 # sit, buy, sell
        
        self.memory4replay = []
        self.is_eval = is_eval

        self.epsilon = 1.0

        self.__chromosome = chromosome
        self.alpha = chromosome.GetAlpha() #########################################
        self.gamma = chromosome.GetGamma() #########################################
        self.epsilon_min = chromosome.GetEpsilonMin() #########################################
        self.epsilon_decay = chromosome.GetEpsilonDecay() #########################################
        
        self.model = self.__model()

    def __layer(self, x, l, dropout):
        if(l.GetType() == 'dense'):
            x = Dense(l.GetUnits(), activation = l.GetActivation())(x)

        elif(l.GetType() == 'lstm'):
            x = Dense(l.GetUnits(), activation = l.GetActivation())(x)

        elif(l.GetType() == 'denseDropout'):
            x = Dense(l.GetUnits(), activation = l.GetActivation())(x)
            x = Dropout(dropout)(x)

        elif(l.GetType() == 'lstmDropout'):
            x = Dense(l.GetUnits(), activation = l.GetActivation())(x)
            x = Dropout(dropout)(x)
        return x

    def __optimizer(self, ty, lr):
        if(ty == 'adam'):
            opt = keras.optimizers.Adam(learning_rate=lr)
        elif(ty == 'rmsprop'):
            opt = keras.optimizers.RMSprop(learning_rate=lr)
        elif(ty == 'sgd'):
            opt = tf.keras.optimizers.SGD(learning_rate=lr)
        return opt

    def __model(self): 
        
        inputs = Input(shape=self.input_shape)        
        x = Flatten()(inputs)

        dropout = self.__chromosome.GetDropout()
        layers = self.__chromosome.GetLayers()

        for k in layers.keys():
            x = self.__layer(x, layers[k], dropout)

        predictions = []
        for i in range(self.portfolio_size): #########################################
            asset_dense = Dense(self.action_size, activation='linear')(x)   
            predictions.append(asset_dense)
        
        model = Model(inputs=inputs, outputs=predictions)


        opt = self.__optimizer(self.__chromosome.GetOptimizer(), self.__chromosome.GetLearningRate())
        model.compile(optimizer = opt, loss = self.__chromosome.GetLoss()) #########################################
        return model

    def nnPredToWeights(self, pred, allow_short = False):

        weights = np.zeros(len(pred))
        raw_weights = np.argmax(pred, axis=-1)

        saved_min = None
        
        for e, r in enumerate(raw_weights):
            if r == 0: # sit
                weights[e] = 0
            elif r == 1: # buy
                weights[e] = np.abs(pred[e][0][r])
            else:
                weights[e] = -np.abs(pred[e][0][r])

        if not allow_short:
            weights += np.abs(np.min(weights))
            saved_min = np.abs(np.min(weights))
            saved_sum = np.sum(weights)
        else:
            saved_sum = np.sum(np.abs(weights))
            
        weights /= saved_sum
        return weights, saved_min, saved_sum
    
    def act(self, state):
        
        if not self.is_eval and random.random() <= self.epsilon:
            w = np.random.normal(0, 1, size = (self.portfolio_size, ))  
              
            saved_min = None
            
            if not self.allow_short:
                w = w + np.abs(np.min(w))
                saved_min = np.abs(np.min(w))
                
            saved_sum = np.sum(w)
            w = w / saved_sum
            return w , saved_min, saved_sum

        pred = self.model.predict(np.expand_dims(state.values, 0))
        return self.nnPredToWeights(pred, self.allow_short)

    def expReplay(self, batch_size):

        for (s, s_, action, reward, done) in self.memory4replay:
            
            action_weights = PortfolioCalculator.restore_Q_from_weights_and_stats(action) 
            Q_learned_value = PortfolioCalculator.weights_to_nn_preds_with_reward(action_weights, 
                                            reward,
                                            portfolio_size = self.portfolio_size,
                                            action_size = self.action_size,
                                            gamma = self.gamma)
            s, s_ = s.values, s_.values    

            if not done:
                # reward + gamma * Q^*(s_, a_)
                Q_star = self.model.predict(np.expand_dims(s_, 0))
                Q_learned_value = PortfolioCalculator.weights_to_nn_preds_with_reward(action_weights, 
                                            reward,
                                            portfolio_size = self.portfolio_size,
                                            action_size = self.action_size,
                                            gamma = self.gamma,
                                            Q_star = np.squeeze(Q_star))  

            Q_learned_value = [xi.reshape(1, -1) for xi in Q_learned_value]
            Q_current_value = self.model.predict(np.expand_dims(s, 0))
            Q = [np.add(q * (1-self.alpha), a * self.alpha) for q, a in zip(Q_current_value, Q_learned_value)]

            self.model.fit(np.expand_dims(s, 0), Q, epochs=1, verbose=0)            
        
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def __del__(self):
        del self.model
        del self.memory4replay
        