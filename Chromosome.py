import numpy as np
import pandas as pd

class Chromosome(object):
    hiddenLayerRate = {'type': 'minMaxFloat', 'min': 0.1, 'max': 0.9, 'decimals': 3}
    hiddenDropout = {'type': 'minMaxFloat', 'min': 0.2, 'max': 0.9, 'decimals': 1}
    ##RL
    alphaRL = {'type': 'minMaxFloat', 'min': 0.1, 'max': 0.9, 'decimals': 3}
    gammaRL = {'type': 'minMaxFloat', 'min': 0.8, 'max': 0.999, 'decimals': 3}
    epsilonMinRL = {'type': 'minMaxFloat', 'min': 0.01, 'max': 0.1, 'decimals': 4}
    epsilonDecayRL = {'type': 'minMaxFloat', 'min': 0.9, 'max': 0.99999, 'decimals': 4}

    def __init__(self):
        self.__fitness = None

        self.__model = None
        self.__history = None

        self.__layers = None
        self.__loss = None
        self.__optimizer = None

        self.__dropoutWS = None ## EE
        self.__learningRateWS = None ## EE
        self.__epsilonMinWS = None ## EE
        self.__epsilonDecayWS = None ## EE
        self.__alphaWS = None ## EE
        self.__gammaWS = None ## EE

    def Reset(self):
        self.__fitness = None

    @staticmethod
    def MapWS(ws, ma):
        return np.round((ma['max']-ma['min']) * ws[0] + ma['min'], ma['decimals'])

    def SetDropoutWS(self, values):
        self.__dropoutWS = values
        self.Reset()

    def GetDropoutWS(self):
        return self.__dropoutWS

    def GetDropout(self):
        return Chromosome.MapWS(self.__dropoutWS, self.hiddenDropout)

    def SetGammaWS(self, values):
        self.__gammaWS = values
        self.Reset()
        
    def GetGammaWS(self):
        return self.__gammaWS

    def GetGamma(self):
        return Chromosome.MapWS(self.__gammaWS, self.gammaRL)

    def SetAlphaWS(self, values):
        self.__alphaWS = values
        self.Reset()

    def GetAlphaWS(self):
        return self.__alphaWS

    def GetAlpha(self):
        return Chromosome.MapWS(self.__alphaWS, self.alphaRL)

    def SetEpsilonDecayWS(self, values):
        self.__epsilonDecayWS = values
        self.Reset()

    def GetEpsilonDecayWS(self):
        return self.__epsilonDecayWS

    def GetEpsilonDecay(self):
        return Chromosome.MapWS(self.__epsilonDecayWS, self.epsilonDecayRL)

    def SetEpsilonMinWS(self, values):
        self.__epsilonMinWS = values
        self.Reset()

    def GetEpsilonMinWS(self):
        return self.__epsilonMinWS

    def GetEpsilonMin(self):
        return Chromosome.MapWS(self.__epsilonMinWS, self.epsilonMinRL)
    
    def SetLearningRateWS(self, values):
        self.__learningRateWS = values
        self.Reset()

    def GetLearningRateWS(self):
        return self.__learningRateWS

    def GetLearningRate(self):
        return Chromosome.MapWS(self.__learningRateWS, self.hiddenLayerRate)

    def SetWS(self, ws, withReset = True):
        self.__dropoutWS = ws[0] ## EE
        self.__learningRateWS = ws[1] ## EE
        self.__epsilonMinWS = ws[2] ## EE
        self.__epsilonDecayWS = ws[3] ## EE
        self.__alphaWS = ws[4] ## EE
        self.__gammaWS = ws[5] ## EE
        if(withReset):
            self.Reset()

    def GetWS(self):
        return np.array([
            self.__dropoutWS,
            self.__learningRateWS,
            self.__epsilonMinWS,
            self.__epsilonDecayWS,
            self.__alphaWS,
            self.__gammaWS
        ])

    def SetW(self, w):
        ws = self.GetWS()
        ws[:,0] = w
        self.SetWS(ws)
        self.Reset()

    def GetW(self):
        ws = self.GetWS()
        return ws[:,0]

    def SetS(self, s):
        ws = self.GetWS()
        ws[:,1] = s
        self.SetWS(ws, withReset = False)

    def GetS(self):
        ws = self.GetWS()
        return ws[:,1]

    def SetLayers(self, values):
        self.__layers = values
        self.Reset()

    def GetLayers(self):
        return self.__layers
    
    def SetLoss(self, values):
        self.__loss = values
        self.Reset()
        
    def GetLoss(self):
        return self.__loss

    def SetOptimizer(self, values):
        self.__optimizer = values
        self.Reset()

    def GetOptimizer(self):
        return self.__optimizer


    def SetFitness(self, values):
        self.__fitness = values

    def GetFitness(self):
        return self.__fitness

    def SetModel(self, values):
        self.__model = values

    def GetModel(self):
        return self.__model

    def SetHistory(self, values):
        self.__history = values

    def GetHistory(self):
        return self.__history

    def Copy(self):
        layers = {}
        for k in self.__layers.keys():
            layers[k] = self.__layers[k].Copy()
        chro = Chromosome()
        chro.SetLayers(layers)
        chro.SetWS(self.GetWS())
        chro.SetLoss(self.GetLoss())
        chro.SetOptimizer(self.GetOptimizer())
        chro.SetFitness(self.GetFitness())
        chro.SetModel(self.GetModel())
        chro.SetHistory(self.GetHistory())
        return chro
        
    def __str__(self):
        
        def getStrLayer(k, v):
            return (f'\nlayer: {k}\n'
                f'type: {v.GetType()}\n'
                f'units: {v.GetUnits()}\n'
                f'activation: {v.GetActivation()}\n')

        layers = '\n'.join([getStrLayer(k, self.__layers[k]) for k in self.__layers.keys()])
        return (
            f'Gamma: {self.GetGamma()}\n'
            f'Alpha: {self.GetAlpha()}\n'
            f'EpsilonMin: {self.GetEpsilonMin()}\n'
            f'EpsilonDecay: {self.GetEpsilonDecay()}\n'
            f'Dropout: {self.GetDropout()}\n'
            f'Optimizer: {self.__optimizer}\n'
            f'LearningRate: {self.GetLearningRate()}\n'
            f'Loss: {self.__loss}\n'
            f'{layers}')

    def __hash__(self):
        return hash(str(self))

    def __del__(self):
        del self.__model
        del self.__history

class ChromosomeLayer(object):
    __type = None
    __activation = None
    __units = None

    def __init__(self):
        pass

    def SetType(self, _type):
        self.__type = _type

    def GetType(self):
        return self.__type

    def SetActivation(self, activation):
        self.__activation = activation

    def GetActivation(self):
        return self.__activation
    
    def SetUnits(self, units):
        self.__units = units

    def GetUnits(self):
        return self.__units

    def Copy(self):
        cp = ChromosomeLayer()
        cp.SetType(self.__type)
        cp.SetActivation(self.__activation)
        cp.SetUnits(self.__units)
        return cp
