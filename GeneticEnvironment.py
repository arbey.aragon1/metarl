class GeneticEnvironment():
    def __init__(self, 
            logger,
            populationManager,
            epocs = 100):   
        self.__logger = logger
        self.__epocs = epocs
        self.__populationManager = populationManager

    def Run(self):
        self.__Start()
        for i in range(self.__epocs):
            self.__logger.debug('Epoch G', str(i)+'/'+str(self.__epocs))
            self.__Update()

    def __Start(self):
        self.__populationManager.start()
        self.__populationManager.generatePopulation()
        self.__populationManager.newGenerationEmpty()
        self.__populationManager.calculateFitnessOld()

    def __Update(self):
        self.__populationManager.calculateFitnessOld()
        self.__populationManager.populationCrossing()
        self.__populationManager.populationMutation()
        self.__populationManager.calculateFitnessNew()
        self.__populationManager.selection()
        
    