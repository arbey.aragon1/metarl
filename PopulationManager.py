import numpy as np
import pandas as pd
import pickle
import multiprocessing
import time

class PopulationManager():
    def __init__(self,
            logger,
            coach,
            chromosomeUtils,
            populationSize = 20,
            percentageMutation = 0.2,
            crossoverPercentage = 0.2,
            debugLevel = 0):
        self.__logger = logger
        self.__debugLevel = debugLevel
        self.__coach = coach
        self.__chromosomeUtils = chromosomeUtils
        self.__populationSize = populationSize
        self.__percentageMutation = percentageMutation
        self.__crossoverPercentage = crossoverPercentage

    def start(self):
        self.count15 = 0
        self.promedioGener = 0
        self.promedioGenerVec = []
        self.sigma = 1.0
        self.hist = []
        self.histFitnessMean = []
        self.histFitnessMedian = []
        self.histFitnessStd = []

    def generatePopulation(self):
        self.__population = []
        for i in range(self.__populationSize):
            self.__population.append(self.__chromosomeUtils.generateChromosome())
        
    def newGenerationEmpty(self):
        self.__newPopulation = []

    def startProcess(self):
        self.__logger.debug('Starting', multiprocessing.current_process().name)

    def calculateFitnessOld(self):
        temp = []
        poputationOut = []
        for i in range(len(self.__population)):
            if(self.__population[i].GetFitness() == None):
                temp.append(self.__population[i])
                #self.__population[i] = self.__coach.trainIndividual(self.__population[i])
            else:
                poputationOut.append(self.__population[i])
        
        start = time.time()
        if(len(temp) > 0):
            pool_size = multiprocessing.cpu_count() * 2
            pool = multiprocessing.Pool(processes=pool_size)
            poputationOut.extend(pool.map(self.__coach.trainIndividual, temp))
            pool.close() # no more tasks
            pool.join()  # wrap up current tasks
            self.__logger.debug('cpu count', pool_size)

        self.__population = poputationOut
        end = time.time()
        self.__logger.debug('Tiempo de calculo de fitness para una generacion old', end - start)
        

    def calculateFitnessNew(self):
        hashTap = {}
        for i in range(len(self.__newPopulation)):
            hashTap[hash(self.__newPopulation[i])] = i

        temp = []
        for k in hashTap.keys():
            i = hashTap[k]
            temp.append(self.__newPopulation[i])
        self.__newPopulation = temp

        temp = []
        poputationOut = []
        for i in range(len(self.__newPopulation)):
            if(self.__newPopulation[i].GetFitness() == None):
                temp.append(self.__newPopulation[i])
                #self.__newPopulation[i] = self.__coach.trainIndividual(self.__newPopulation[i])
            else:
                poputationOut.append(self.__newPopulation[i])
        

        start = time.time()
        if(len(temp) > 0):
            pool_size = multiprocessing.cpu_count() * 2
            pool = multiprocessing.Pool(processes=pool_size)
            poputationOut.extend(pool.map(self.__coach.trainIndividual, temp))
            pool.close() # no more tasks
            pool.join()  # wrap up current tasks
            self.__logger.debug('cpu count', pool_size)
        self.__newPopulation = poputationOut
        end = time.time()
        self.__logger.debug('Tiempo de calculo de fitness para una generacion new', end - start)
        
    def populationCrossing(self):
        le = len(self.__population)
        for i in range(int(self.__populationSize * self.__crossoverPercentage)):
            ks = np.random.choice(le, 2, replace=False)
            p1 = self.__population[ks[0]]
            p2 = self.__population[ks[1]]
            child1, child2 =  self.__chromosomeUtils.crossChromosomes(p1, p2)
            self.__newPopulation.extend([child1, child2])

    def populationMutation(self):
        le = len(self.__population)
        for i in range(int(self.__populationSize * self.__percentageMutation)):
            p1 = self.__population[np.random.choice(le, 1)[0]]
            
            child =  self.__chromosomeUtils.mutationChromosomes(p1)
            
            self.__newPopulation.extend([child])
    
    def selection(self):
        self.__population = self.__population[:5]

        def keyValue(elem):
            return -elem.GetFitness()
        self.__newPopulation.sort(key=keyValue)

        promedio = 0
        nPop = min([int(self.__populationSize), len(self.__newPopulation)])
        fitnessList = []
        for i in range(nPop):
            self.__population.append(self.__newPopulation[i]);
            fitnessList.append(self.__newPopulation[i].GetFitness())
        
        promedio=np.mean(fitnessList)
        self.count15 += 1.0
        self.promedioGenerVec.append(promedio)

        if(self.__debugLevel >= 1):
            print('The best: {:.2f}'.format(self.__population[0].GetFitness()))
       
        if(int(self.count15) % 1 == 0):
            median = np.median(fitnessList)
            desv = np.std(fitnessList)

            self.histFitnessMedian.append(median)
            self.histFitnessMean.append(promedio)
            self.histFitnessStd.append(desv)
            self.hist.append(fitnessList)
            
            self.__logger.debug('std', str(desv))
            self.__logger.debug('median', str(median))
            self.__logger.debug('mean', str(promedio))
            self.__logger.debug('the best', str(self.__population[0].GetFitness()))
            
            pickle.dump(self.histFitnessMean, open( "dataOut/histM.p", "wb" ) )
            pickle.dump(self.histFitnessMedian, open( "dataOut/histMedian.p", "wb" ) )
            pickle.dump(self.histFitnessStd, open( "dataOut/histS.p", "wb" ) )
            
            pickle.dump(self.hist, open( "dataOut/hist.p", "wb" ) )
            
        if(int(self.count15) % 10 == 0):
            pickle.dump(self.__newPopulation[:10], open( "dataOut/indvList.p", "wb" ) )

        ##regla 1/5
        if(self.count15 > 100):
            
            self.count15=0

            promedioGener = np.mean(self.promedioGenerVec)

            ps = promedio / promedioGener

            if(ps>(1.0-1.0/5.0)):
                sigma=0.817;
            elif(ps<(1.0/5.0)):
                sigma=1.0/0.817;
            elif(ps==(1.0/5.0)):
                sigma=1.0;
            
            self.promedioGenerVec = []
            
        
        #for i in np.random.choice(len(self.__population), 3):
        #    self.__population[i].Reset()

        for i in range(len(self.__population)):
            self.__population[i].SetS(self.__population[i].GetS() * self.sigma)

        self.__newPopulation = []