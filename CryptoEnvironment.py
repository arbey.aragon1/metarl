import numpy as np
import pandas as pd
from Environment import Environment

class CryptoEnvironment(Environment):  
    def __init__(self, prices = './data/crypto_portfolio.csv', capital = 1e6):       
        self.prices = prices  
        self.capital = capital  
        self.data = self.load_data()

    def load_data(self):
        data =  pd.read_csv(self.prices)
        try:
            data.index = data['Date']
            data = data.drop(columns = ['Date'])
        except:
            data.index = data['date']
            data = data.drop(columns = ['date'])            
        return data
    
    def get_reward(self, action, t_1, t):
        ret, sharpe, rew = self.get_reward_prev(action, t_1, t)
        return ret, rew