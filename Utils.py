import numpy as np
class Utils():

    #### Generators
    @staticmethod
    def GenerateWeights(length):
        return np.random.uniform(low=0.0, high=1.0, size=(length,))
    
    @staticmethod
    def GenerateSigmas(length):
        return np.random.uniform(low=0.0, high=0.1, size=(length,))

    @staticmethod
    def GenerateIntArray(length, minVal, maxVal):
        return np.random.randint(minVal, maxVal + 1, size=(length,))
        
    @staticmethod
    def GenerateChoice(length, vector):
        return np.random.choice(vector, length)

    #### Crosses
    @staticmethod
    def MeanCrossing(p1, p2):
        return (p1 + p2)*0.5

    @staticmethod
    def GeneExchangeCrossover(p1, p2):
        a = np.random.randint(0, 2, size=(len(p1),))
        b = (a-1)*-1
        return p1*a + p2*b, p1*b + p2*a

    @staticmethod
    def MutationEE(weight, standardv):
        return np.clip(np.concatenate([np.random.normal(m, s, 1) for m, s in zip(weight, standardv)]), 0, 1)  
