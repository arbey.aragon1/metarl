import numpy as np
import pandas as pd
import tensorflow  as tf

class MarkowitzOptimizer:
    def __init__(self, learningRate, portfolioSize, idOptimizer=0, steps = 500):
        self.idOptimizer = idOptimizer
        self.steps = steps
        self.portfolioSize = portfolioSize
        self.learningRate =  learningRate
        self.__optimizer = self.__selectOptimizer()
        self.returns = None

    def localPortfolio(self, returns, weights):
        rets = returns.mean() # * 252
        covs = returns.cov() # * 252
        P_ret = tf.tensordot(rets, weights, 1)

        temporal2 = tf.tensordot(covs, weights, 1)
        temporal1 = tf.tensordot(tf.transpose(weights),temporal2, 1)
        P_vol = tf.sqrt(temporal1)
        P_sharpe = P_ret / P_vol
        return [P_ret, P_vol, P_sharpe]

    
    def lossFunction(self):
        st = self.localPortfolio(self.returns, self.coinWeights)
        return -st[2]

    def getWeights(self):
        return self.coinWeights.numpy()
        
    def trainStep(self, loss):
        step_count = self.__optimizer.minimize(loss, self.coinWeights).numpy()
        
        #weights_sum = tf.reduce_sum(self.coinWeights)
        #self.coinWeights.assign(tf.divide(tf.abs(self.coinWeights), tf.abs(weights_sum) ))
        
        weights_sum = tf.reduce_sum(tf.abs(self.coinWeights))
        self.coinWeights.assign(tf.divide(self.coinWeights, tf.abs(weights_sum) ))
        
        
        #print(np.ones(self.portfolioSize)/float(self.portfolioSize))
        #print(PortfolioCalculator.local_portfolio(self.returns,self.getWeights()))
        #print(self.getWeights())

    def train(self, returns):
        self.returns = returns
        loss = lambda: self.lossFunction()
        self.coinWeights = tf.Variable(np.ones(self.portfolioSize)/float(self.portfolioSize))
        for i in range(self.steps):
            self.trainStep(loss)
        self.returns = None

    def getOptimizerName(self):
        return self.optimizerName

    def __selectOptimizer(self):
        method_name = 'optimizer_' + str(self.idOptimizer)
        method = getattr(self, method_name, lambda: "Invalid function")
        return method()
        
    def optimizer_0(self):
        self.optimizerName = "SGD"
        return tf.keras.optimizers.SGD(self.learningRate)
 
    def optimizer_1(self):
        self.optimizerName = "RMSprop"
        return tf.keras.optimizers.RMSprop(self.learningRate, rho=0.9 ,momentum=0.0, epsilon=1e-07, centered=False)
    
    def optimizer_2(self):
        self.optimizerName = "Adam"
        return tf.keras.optimizers.Adam(self.learningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, amsgrad=False)

    def optimizer_3(self):
        self.optimizerName = "Adadelta"
        return tf.keras.optimizers.Adadelta(self.learningRate, rho=0.95, epsilon=1e-07)

    def optimizer_4(self):
        self.optimizerName = "Adagrad"
        return tf.keras.optimizers.Adagrad(self.learningRate, initial_accumulator_value=0.1, epsilon=1e-07)

    def optimizer_5(self):
        self.optimizerName = "Adamax"
        return tf.keras.optimizers.Adamax(self.learningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-07)

    def optimizer_6(self):
        self.optimizerName = "Nadam"
        return tf.keras.optimizers.Nadam(self.learningRate, beta_1=0.9, beta_2=0.999, epsilon=1e-07)

    #def optimizer_7(self):
    #    self.optimizerName = "Ftrl"
    #    return tf.keras.optimizers.Ftrl(self.learningRate, 
    #                                    learningRate_power=-0.5, 
    #                                    initial_accumulator_value=0.1, 
    #                                    l1_regularization_strength=0.0, 
    #                                    l2_regularization_strength=0.0, 
    #                                    l2_shrinkage_regularization_strength=0.0)
