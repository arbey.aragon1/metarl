import numpy as np
import pandas as pd

class PortfolioCalculator:
    @staticmethod
    def annualizedPortfolio(returns, weights):
        weights = np.array(weights)
        rets = returns.mean() * 252
        covs = returns.cov() * 252
        P_ret = np.sum(rets * weights)
        P_vol = np.sqrt(np.dot(weights.T, np.dot(covs, weights)))
        P_sharpe = P_ret / P_vol
        return np.array([P_ret, P_vol, P_sharpe])

    @staticmethod
    def sharpe(R):
        r = np.diff(R)
        sr = r.mean()/r.std() * np.sqrt(252)
        return sr

    @staticmethod
    def local_portfolio(returns, weights):
        weights = np.array(weights)
        rets = returns.mean() # * 252
        covs = returns.cov() # * 252
        P_ret = np.sum(rets * weights)
        P_vol = np.sqrt(np.dot(weights.T, np.dot(covs, weights)))
        P_sharpe = P_ret / P_vol
        return np.array([P_ret, P_vol, P_sharpe])
 
    @staticmethod
    def restore_Q_from_weights_and_stats(action):            
        action_weights, action_min, action_sum = action[0], action[1], action[2]
        action_weights = action_weights * action_sum          
        if action_min != None:
            action_weights = action_weights - action_min   
        return action_weights

    @staticmethod
    def weights_to_nn_preds_with_reward(action_weights, 
                                  reward,
                                  portfolio_size,
                                  action_size, 
                                  gamma,
                                  Q_star = []):

        Q = np.zeros((portfolio_size, action_size))           
        if len(Q_star) == 0:
            Q_star = np.zeros((portfolio_size, action_size))

        for i in range(portfolio_size):
            if action_weights[i] == 0:
                Q[i][0] = reward[i] + gamma * np.max(Q_star[i][0])
            elif action_weights[i] > 0:
                Q[i][1] = reward[i] + gamma * np.max(Q_star[i][1])
            else:
                Q[i][2] = reward[i] + gamma * np.max(Q_star[i][2])            
        return Q 
