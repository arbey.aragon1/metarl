import numpy as np
import pandas as pd
from Environment import Environment

class ETFEnvironment(Environment):
    def __init__(self, volumes = './data/volumes.txt',
                       prices = './data/prices.txt',
                       returns = './data/returns.txt', 
                       capital = 1e6):
        
        self.returns = returns
        self.prices = prices
        self.volumes = volumes   
        self.capital = capital  
        self.data = self.load_data()

    def load_data(self):
        volumes = np.genfromtxt(self.volumes, delimiter=',')[2:, 1:]
        prices = np.genfromtxt(self.prices, delimiter=',')[2:, 1:]
        returns=pd.read_csv(self.returns, index_col=0)
        assets=np.array(returns.columns)
        dates=np.array(returns.index)
        return pd.DataFrame(prices, 
             columns = assets,
             index = dates
            )
        
    def get_reward(self, action, t_1, t):
        ret, sharpe, rew = self.get_reward_prev(action, t_1, t)
        return ret, rew