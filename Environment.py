import numpy as np
import pandas as pd
from PortfolioCalculator import PortfolioCalculator

class Environment:
    def preprocessState(self, state):
        return state
        
    def getState(self, t, lookback, is_cov_matrix = True, is_raw_time_series = False):
        
        assert lookback <= t
        
        time_window = self.data.iloc[t-lookback:t]
        
        if is_raw_time_series:
            return time_window
        else:
            returns = time_window.pct_change().dropna()
            if is_cov_matrix:
                return returns.cov()        
            return returns

    def getRewardPrev(self, action, t_1, t):
        data_period = self.data[t_1:t]
        weights = action
        returns = data_period.pct_change().dropna()
      
        sharpe = PortfolioCalculator.local_portfolio(returns, weights)[-1]
        sharpe = np.array([sharpe] * len(self.data.columns))     
        rew = (data_period.values[-1] - data_period.values[0]) / data_period.values[0] 
        return np.dot(returns, weights), sharpe, rew
