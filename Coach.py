import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pylab as plt
import time

from keras.layers import Input, Dense, Flatten, Dropout
from keras.models import Model
from keras.regularizers import l2

from Agent import Agent

sns.set(style="whitegrid")

class Coach():
    __instance = None

    def __init__(self, 
            logger, 
            env, 
            epochs, 
            benchmark = None, 
            debugLevel = 0, 
            debugFunction = False, 
            debugInterval = 10):
        self.__debugLevel = debugLevel
        self.__debugInterval = debugInterval
        self.__debugFunction = debugFunction
        self.__epochs = epochs
        self.__env = env
        self.__logger = logger

        self.__nAssets = 32
        self.__windowSize = 120
        self.__rebalancePeriod = 30
        self.__episodeCount = 200
        self.__batchSize = 32
        self.__dataLength = len(env.data)
        self.__benchmark = benchmark

    @staticmethod
    def getInstance(logger, env, epochs):
        if Coach.__instance == None:
            Coach.__instance = Coach(logger, env, epochs)
        return Coach.__instance


    def __train(self, chromosome):
        scoreHist = []
        agent = Agent(self.__nAssets, chromosome)        
        for e in range(self.__epochs):  #### Iteraciones de RL
            agent.is_eval = False
            returns_history = []
            rewards_history = []
            actions_to_show = []
            
            ##### return the matrix of cov
            s = self.__env.getState( \
                np.random.randint( \
                    self.__windowSize + 1, self.__dataLength - self.__windowSize - 1), \
                self.__windowSize) 
            
            total_profit = 0 

            for t in range(self.__windowSize, self.__dataLength, self.__rebalancePeriod): ### Iteraciones de la ventana de tiempo 

                ## t-1
                t_1 = t - self.__rebalancePeriod
                
                ##### return the matrix of cov
                s_ = self.__env.getState(t, self.__windowSize)
                action = agent.act(s_)
                
                actions_to_show.append(action[0])

                weighted_returns, reward = self.__env.getReward(action[0], t_1, t)
                
                rewards_history.append(reward)
                returns_history.extend(weighted_returns)
                
                done = (t == self.__dataLength)
                agent.memory4replay.append((s, s_, action, reward, done))
                
                if len(agent.memory4replay) >= self.__batchSize:
                    agent.expReplay(self.__batchSize)
                    agent.memory4replay = []        
                s = s_
            
            rl_result = np.array(returns_history).cumsum()
            
            #################
            score = np.sum(rl_result)
            scoreHist.append(score)
            
            if((e % self.__debugInterval == 0) and (self.__debugLevel >= 2)):
                print("Episode " + str(e) + "/" + str(self.__epochs), 'epsilon', agent.epsilon)
                print(score)
                plt.figure(figsize = (12, 2))
                plt.plot(rl_result, color = 'black', ls = '-')
                if(self.__benchmark != None):
                    plt.plot(self.__benchmark, color = 'blue', ls = '--')
                plt.show()

        score = np.sum(rl_result)
        if(self.__debugLevel >= 1):
            plt.plot(scoreHist)
            plt.show()
        del agent
        return score, scoreHist
                
    def __trainIndividualMock(self, chromosome):
        f = np.sum(chromosome.GetW())
        f += [0,1][chromosome.GetLoss() == 'mean_squared_error']
        f += [0,1][chromosome.GetOptimizer() == 'adam']
        layers = chromosome.GetLayers()
        f += len(layers.keys())
        for k in layers.keys():
            f += layers[k].GetUnits()
            f += [0,1][layers[k].GetActivation() == 'sigmoid']
            f += [0,1][layers[k].GetType() == 'dense']
        
        
        chromosome.SetFitness(f)
        chromosome.SetHistory(None)
        return chromosome

    def __trainIndividual(self, chromosome):
        fitnessVec = []
        for i in range(5):
            try:
                fitness, history = self.__train(chromosome)
                if(np.isnan(fitness)):
                    fitness = 0
            except Exception as e:
                print(e)
                fitness = 0
            fitnessVec.append(fitness)

        chromosome.SetFitness(np.mean(fitnessVec))
        chromosome.SetHistory(None)
        
        return chromosome

    def trainIndividual(self, chromosome):
        if(self.__debugFunction):
            self.__trainIndividualMock(chromosome)
        else:
            self.__trainIndividual(chromosome)
        return chromosome