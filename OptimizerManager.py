import numpy as np
import pandas as pd
import matplotlib.pylab as plt
from MarkowitzOptimizer import MarkowitzOptimizer

class OptimizerManager:
    def __init__(self, portfolioSize):
        self.portfolioSize = portfolioSize
        self.ids = list(range(7))
        self.learningRate = 0.01
        self.steps = 100
        self.currentId = None                
        self.methods = {id: MarkowitzOptimizer(self.learningRate,self.portfolioSize,id, steps=self.steps) for id in self.ids}
        self.statistics = {id:{'returns_history_equal': [], 'equal_rewards': []} for id in self.ids}

    def nextMethod(self):
        for i in self.ids:
            self.currentId = i
            yield i

    def train(self, returns):
        self.methods[self.currentId].train(returns)

    def getWeights(self):
        return self.methods[self.currentId].getWeights()

    def getOptimizerName(self):
        return self.methods[self.currentId].getOptimizerName()

    def setStatistics(self, weighted_returns_equal, reward_equal):
        self.statistics[self.currentId]['returns_history_equal'].extend(weighted_returns_equal) 
        self.statistics[self.currentId]['equal_rewards'].append(reward_equal)

    def plot(self):
        data = {self.methods[id].getOptimizerName(): np.array(self.statistics[id]['returns_history_equal']).cumsum() for id in self.ids}
        pd.DataFrame(data).plot()
        plt.show()